package b137.obrera.s02a1;

import java.util.Scanner;

public class LeapYearCalculator {

    public static void main(String[] args){

        System.out.println("Leap Year Calculator\n");

        Scanner appScanner = new Scanner(System.in);

        System.out.println("What is your firstname?\n");
        String firstName = appScanner.nextLine();
        System.out.println("Hello, " + firstName + "!\n");

        // Activity: Create a program that check if a year is a leap year or not.

        Scanner appChecker = new Scanner(System.in);

        System.out.println("What year are you checking?\n");
        int year = appChecker.nextInt();
        boolean leap = false;

        if (year % 4 == 0){
            if (year % 100 == 0){
                if (year% 400 == 0)
                    leap = true;
                else
                    leap = false;
            }
            else
                leap = true;
        }
         else
             leap = false;

         if (leap)
             System.out.println(year + " is a leap year.");
         else
             System.out.println(year + " is not a leap year");
    }
}
